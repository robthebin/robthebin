from flask import Flask, render_template, url_for, redirect
from config import Config
from database import db_session, init_db
from registration_form import RegistrationForm
from models import *

app = Flask(__name__)
app.config.from_object(Config)


@app.route('/register', methods=['GET', 'POST'])
def create_student():
    form = RegistrationForm()
    # extract (value, label) pairs for WTForms
    # refer to https://wtforms.readthedocs.io/en/stable/fields.html#field-definitions
    form.course.choices = [(row.course_id, row.course_name) for row in Course.query.all()]
    if form.validate_on_submit():
        s = Student(
            form.data['firstname'],
            form.data['lastname'],
            form.data['gender'],
            form.data['residential_address'],
            form.data['phone_number'],
            form.data['course']
        )
        db_session.add(s)
        db_session.commit()
        return redirect(url_for('registration_complete'))

    return render_template("create_student.html", title="Student Registration", form=form)


@app.route('/thanks')
def registration_complete():
    return render_template("thanks.html", title="Registration Complete")


# disconnect from the database upon app closing
@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == '__main__':
    init_db()
    app.run()

