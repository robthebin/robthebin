import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'robin'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              "mysql://robin:robin@localhost:3306/robin" # remember to change before upload
    SQLALCHEMY_TRACK_MODIFICATIONS = False
