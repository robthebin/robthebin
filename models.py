from database import Base
from sqlalchemy import Column, Integer, String

# SQLalchemy is a ORM: Object Relational Mapper
# What it does is that is maps (translates) an object from its representation in python to SQL and vice versa

class Course(Base):
    __tablename__ = 'CourseMaster'
    course_uid = Column(Integer, primary_key=True, autoincrement=True)
    course_id = Column(String(10), unique=True)
    course_name = Column(String(50))
    course_text = Column(String(255))

    def __init__(self, id, name, description):
        self.course_id = id
        self.course_name = name
        self.course_text = description

    def __repr__(self):
        return 'Course UID: {}\nCourse ID: {}\nCourse Name:{}\nCourse Desc:{}' \
            .format(self.course_uid, self.course_id, self.course_name, self.course_text)


class Student(Base):
    __tablename__ = 'StudentMaster'

    student_id = Column(Integer, primary_key=True, autoincrement=True)
    student_firstname = Column(String(50))
    student_lastname = Column(String(50))
    student_gender = Column(String(10))
    student_address = Column(String(256))
    student_phone = Column(String(11))
    course_id = Column(String(10))

    def __init__(self, first_name, last_name, gender, address, phone, course_id):
        self.student_firstname = first_name
        self.student_lastname = last_name
        self.student_gender = gender
        self.student_address = address
        self.student_phone = phone
        self.course_id = course_id

    def __repr__(self):
        return 'ID: {}\nFirst: {}\nLast: {}\nGender: {}\nAddress: {}\nPhone: {}\nCourse: {}' \
            .format(self.student_id, self.student_firstname, self.student_lastname, self.student_gender,
                    self.student_address, self.student_phone, self.course_id)
